Name: Amen Ahmed Elbalal
Course: PDE4430 mobile robotics
MISIS: M00910292

Coursework 2 Mobile Robot on Gazebo and Rviz

# kaiba pkg

### 1. Installation
```
cd ~/catkin_ws/src/
git clone https://gitlab.com/Amen95/pde_4430_cw.git
cd catkin_ws
catkin_make
```

### 2. Simulation
 
#### 2.1. World simulation
Assessment world gazebo is attached with the robot spawn launch , Run below command to open virtual house environment. 
```
roslaunch kaiba_description kaiba.launch
```

In order to teleoperate the robot with the keyboard,  launch the teleoperation node with below command in a new terminal window.
```
roslaunch kaiba_teleop kaiba_teleop_key.launch
```

#### 2.2. SLAM simulation
The following instructions require prerequisites from the previous section. 
Open a new terminal window from the PC and run  the SLAM node. Gmapping SLAM method is used by default. 
```
roslaunch kaiba_slam kaiba_gmapping.launch.
```
When the map is created successfully, open a new terminal from remote PC and save the map.
```
rosrun map_server map_saver -f ~/ros/catkin_ws/src/pde_4430_cw/kaiba_navigation/maps/map
 ``` 
#### 2.3. Navigation simulation
Just like the SLAM simulation, Navigation simulation also requires prerequisites from world simulation section. 
Open a new terminal and run the Navigation node.
```
roslaunch kaiba_navigation kaiba_navigation.launch
 
 


